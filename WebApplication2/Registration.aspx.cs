﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;

namespace WebApplication2
{
    public partial class Registration : System.Web.UI.Page
    {
        SqlConnection sqlcon = new SqlConnection(ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (sqlcon.State == System.Data.ConnectionState.Closed)
            {
                sqlcon.Open();
            }
            SqlCommand sqlcmd = new SqlCommand("insert into registration(s_name,addr)values(@s_name,@addr)",sqlcon);
            
            sqlcmd.Parameters.AddWithValue("@s_name", txtsname.Text.Trim());
            sqlcmd.Parameters.AddWithValue("@addr", txtadd.Text.Trim());
            sqlcmd.ExecuteNonQuery();

            SqlDataAdapter sda = new SqlDataAdapter("Select * from registration",sqlcon);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            Repeater.DataSource = dt;
            Repeater.DataBind();
            sqlcon.Close();
        }
    }
}