﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Registration.aspx.cs" Inherits="WebApplication2.Registration" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div style="text-align:center">
            <asp:Label ID="Label1" runat="server" Text=" Registration Form"></asp:Label>
        </div>
        <div>
            <center>
            <table>
                <tr>
                    <td>
                        <asp:Label ID="Label2" runat="server" Text="Student Name"></asp:Label>
                        <asp:TextBox ID="txtsname" runat="server"></asp:TextBox>
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <asp:Label ID="Label4" runat="server" Text="Address"></asp:Label>
                        <asp:TextBox ID="txtadd" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" />
                  
                        <asp:Button ID="btnUpdate" runat="server" Text="Update" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblSuccess" runat="server"></asp:Label>
                        <asp:Label ID="lblnot" runat="server"></asp:Label>
                    </td>
                    
                </tr>
            </table>
            <asp:Repeater ID="Repeater" runat="server">
                   <HeaderTemplate> All Result</HeaderTemplate>
                    <ItemTemplate>
                        <table class="border">
                           
                            <tr>
                                <td>Student ID</td><td><%#Eval("id") %></td>
                              
                            </tr>
                            <tr>
                                <td>Student Name</td><td><%#Eval("s_name") %></td>
                            </tr>
                            <tr>
                                <td>Student Address</td><td><%#Eval("addr") %></td>
                            </tr>
                        </table>
                    </ItemTemplate>
            </asp:Repeater>
            </center>
        </div>
    </form>
</body>
</html>
